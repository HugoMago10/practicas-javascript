//no puedo reutilizarlo
const cliente ={
    nombre : 'Juan',
    saldo:550
}

console.log (cliente);
console.log (typeof cliente);

//si puedo reutilizarlo
function Cliente(nombre, saldo){
    this.nombre = nombre;
    this.saldo = saldo;
}

const juan = new Cliente("Pedro",330);
console.log (juan);