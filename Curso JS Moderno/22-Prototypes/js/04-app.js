function Cliente (nombre, saldo){
    this.nombre = nombre;
    this.saldo = saldo;
}
//crea un nuevo metodo con prototype para todos los objetos que se instancien de Cliente
Cliente.prototype.tipoCliente = function(){
    let tipo;
    //creando tipo de acuerdo al saldo del cliente
    if (this.saldo > 2500){
        tipo = 'Cliente Platinum';
        if (this.saldo > 5000)
            tipo = 'Cliente Gold';
    }else{
        tipo = 'Cliente Standard';
    }
    return tipo; //asigna saldo al tipoCliente;
}

Cliente.prototype.toString = function toString(){
    return `${this.nombre} ${this.saldo} ${this.tipoCliente()}`;
}

lucia = new Cliente("Lucia",5684);

console.log ( lucia.toString() )

//herencia
function ClientePreferido (nombre, saldo, telefono){
    Cliente.call(this, nombre, saldo)
    this.telefono = telefono;
}

const vane = new ClientePreferido("Vane", 5410,'15998741');
console.log (vane);