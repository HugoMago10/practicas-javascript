function Cliente (nombre, saldo){
    this.nombre = nombre;
    this.saldo = saldo;
}
//crea un nuevo metodo con prototype para todos los objetos que se instancien de Cliente
Cliente.prototype.tipoCliente = function(){
    let tipo;
    //creando tipo de acuerdo al saldo del cliente
    if (this.saldo > 2500){
        tipo = 'Cliente Platinum';
        if (this.saldo > 5000)
            tipo = 'Cliente Gold';
    }else{
        tipo = 'Cliente Standard';
    }
    return tipo; //asigna saldo al tipoCliente;
}

const pedro = new Cliente('Pedro',1621);//pedro tiene el metodo de tipoCliente
console.log(pedro, pedro.tipoCliente());

const diana = new Cliente('Diana',5621);//diana igual tiene el metodo de tipoCliente
console.log(diana, diana.tipoCliente());

//creando un atributo para Cliente
Cliente.prototype.compaiña = 'StudiosJorguer';

//cambiando empresa a pedro
pedro.compaiña = 'StudiosKimApp';
console.log (`${diana.nombre} trabaja en ${diana.compaiña}`)
console.log (`${pedro.nombre} trabaja en ${pedro.compaiña}`)

