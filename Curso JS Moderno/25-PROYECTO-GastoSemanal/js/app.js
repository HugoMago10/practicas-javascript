//variables y selectores
const formulario = document.querySelector("#agregar-gasto");
const gastoListado = document.querySelector("#gastos ul");


//eventos
eventos();
function eventos() {

    document.addEventListener("DOMContentLoaded", preguntarPresupuesto);
    //evento al formulario
    formulario.addEventListener("submit", agregaGasto);
}

//clases
class Presupuesto {
    constructor(cantidad) {
        this._presupuesto = parseFloat(cantidad);
        this._restante = parseFloat(cantidad);
        this._gastos = [];//contiene los objectos gasto con nombre, gasto e id
    }

    addGastos(gasto) {
        this._gastos.push(gasto);
        this.calculaRestante();
    }

    removeGasto(id) {
        this._gastos = this._gastos.filter(x => x.id != id)
        this.calculaRestante();
    }

    get presupuesto() {
        return this._presupuesto;
    }

    calculaRestante() {
        //calcula la cantidad de dinero que se va gastando con los producto registrados
        const gastado = this._gastos.reduce((acumulador, elemento) => acumulador + elemento.cantidad, 0);
        this._restante = this.presupuesto - gastado;
    }
}

class UI {
    constructor(objetoPresupuesto) {
        this._objetoPresupuesto = objetoPresupuesto;
    }

    dibujaHtml() {
        const { _presupuesto, _restante } = this._objetoPresupuesto;
        document.querySelector("#total").textContent = _presupuesto;
        document.querySelector("#restante").textContent = _restante;
        this.compruebaPresupuesto();
        this.agregaListadoHtml();

    }

    alerta(mensaje, tipo) {
        const divMensaje = document.createElement('div');
        divMensaje.classList.add('text-center', 'alert');
        if (tipo === 'error') {
            divMensaje.classList.add('alert-danger');
        } else {
            divMensaje.classList.add('alert-success');
        }

        //mensaje de error
        divMensaje.textContent = mensaje;

        //inserta en html
        document.querySelector('.primario').insertBefore(divMensaje, formulario);

        setTimeout(() => {
            //elimina alerta en 2seg
            divMensaje.remove();
        }, 2000)
    }

    compruebaPresupuesto() {
        const { _presupuesto, _restante } = this._objetoPresupuesto;
        const restanteDiv = document.querySelector('.restante');

        if (_presupuesto / 4 > _restante) {//si se gasto mas del 75%
            restanteDiv.classList.remove('alert-success', 'alert-warning');
            restanteDiv.classList.add('alert-danger');
        } else if ((_presupuesto / 2) > _restante) {
            restanteDiv.classList.remove('alert-success');
            restanteDiv.classList.add('alert-warning');
        }
        else {
            restanteDiv.classList.remove('alert-danger', 'alert-warning');
            restanteDiv.classList.add('alert-success');
        }
        //si total es cero
        if (_restante <= 0) {
            this.alerta('El presupuesto se ha agostado', 'error');
            document.querySelector('button[type="submit"]').disabled = true;
        }
    }

    agregaListadoHtml() {
        this.limpiaHtml();

        //Iterando sobre los gastos del arrego del objetp
        this._objetoPresupuesto._gastos.forEach(ele => {
            const { nombre, cantidad, id } = ele;
            //crea li
            const nuevoGasto = document.createElement('li');
            nuevoGasto.className = 'list-group-item d-flex justify-content-between align-items-center';
            /* nuevoGasto.setAttribute('data-id', id); */
            nuevoGasto.dataset.id = id;

            //agrega html hay que tener en cuenta que innerHtml es un poco inseguro
            nuevoGasto.innerHTML = `${nombre}<span class='badge badge-primary badge-pill'>$${cantidad}</span>`

            //boton para borrar gasto
            const btnBorrar = document.createElement('button');
            btnBorrar.classList.add('btn', 'btn-danger', 'borrar-gasto');
            btnBorrar.onclick = () => {
                this._objetoPresupuesto.removeGasto(id);
                console.log(this._objetoPresupuesto);
                //this.agregaListadoHtml();
                this.dibujaHtml()
            }
            btnBorrar.innerHTML = 'borrar &times;'
            nuevoGasto.appendChild(btnBorrar);

            //agrega html
            gastoListado.appendChild(nuevoGasto);
        })
    }

    limpiaHtml() {
        while (gastoListado.firstChild) {
            gastoListado.removeChild(gastoListado.firstChild);
        }
    }
}

//instancias globales
let ui;
let presupuesto;



// ******************* funciones ****************************

function preguntarPresupuesto() {//se encarga de preguntar el presupuesto
    const cantidadPre = parseFloat(prompt("Cual es tu presupuesto"))

    if (cantidadPre === '' || cantidadPre === null || Number.isNaN(cantidadPre) || cantidadPre <= 0) {
        window.location.reload();//recarga la ventana actual
    } else {
        presupuesto = new Presupuesto(cantidadPre);
        //presupuesto valido
        ui = new UI(presupuesto);
        ui.dibujaHtml();
    }
}

function agregaGasto(e) {
    e.preventDefault();

    //leyendo los datos del formulario
    const nombre = document.querySelector("#gasto").value;
    const cantidad = parseFloat(document.querySelector("#cantidad").value);

    //validar
    if (nombre === '' || cantidad === '') {
        ui.alerta('ambos campos son obligatorios', 'error');
        return;//corta el flujo ya no sigue
    } else {
        if (cantidad <= 0 || Number.isNaN(cantidad)) {
            ui.alerta("Cantidad no valida", 'error')
            return;
        }
    }
    console.log("agregando gastos");

    //genera un objeto de gasto -> objeto literal
    const gasto = { nombre, cantidad, id: Date.now() };
    presupuesto.addGastos(gasto);

    ui.alerta('Gasto agregado correctamente');
    formulario.reset();

    //ui.agregaListadoHtml();
    ui.dibujaHtml();

}