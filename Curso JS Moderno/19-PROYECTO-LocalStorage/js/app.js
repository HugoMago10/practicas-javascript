//Variables

const formulario = document.querySelector("#formulario");
const listaTwet = document.querySelector("#lista-tweets");
let twet = [];

//EventListener
eventListener();

//Funciones

function eventListener() {
    formulario.addEventListener('submit', agregaTweet);

    //evneto cuando se carge la pagina completamente
    window.addEventListener("load", () => {
        //cuando el documento este listo se leen los tweet desde el localstorage
        twet = JSON.parse(localStorage.getItem('tweet')) || []//si es null regresame un arreglo vacio
        getFocus();
        crearHtml();
    });
}

function agregaTweet(e) {
    e.preventDefault();

    //text area donde el usuario escribe 
    const tweet = document.getElementById("tweet").value;

    //validacioones
    if (tweet === '') {
        mostrarError("El campo no puede ir vacio");
        getFocus();
    } else {
        const tweetObjeto = {
            id: Date.now(), //sera el identificador del objeto
            tweet //sera el nombre, y el valor otra forma -> tweet : tweet
        }

        //añadiendo al arreglo el objeto tweet
        twet = [...twet, tweetObjeto];
        limpiarHtml();
        crearHtml();

        //reiniciar el formulario
        formulario.reset();
        getFocus();
    }
}

function mostrarError(error) {
    const p = document.createElement('p')
    p.textContent = error;
    p.classList.add('error');
    const contenido = document.querySelector("#contenido");
    contenido.appendChild(p);

    //agrega un mensaje pero despues de 3s quiero que se elimine la alerta
    setTimeout(() => {
        p.remove();
    }, 2000)
}

function crearHtml() {
    //se recorre el arreglo de objetos twee
    twet.forEach(t => {
        //creando el html
        const btnEliminar = document.createElement("a");
        btnEliminar.classList.add('borrar-tweet')
        btnEliminar.textContent ='x';
        //eventoBtnEliminar
        btnEliminar.onclick = ()=>{
            borrarTweet(t.id);
        }


        const li = document.createElement('li');
        li.innerText = t.tweet;
        li.appendChild(btnEliminar);

        listaTwet.appendChild(li);
    });
    sincronizarStorage();
}

function limpiarHtml() {
    while (listaTwet.firstChild) {
        listaTwet.removeChild(listaTwet.firstChild);
    }
}

function getFocus() {
    document.getElementById("tweet").focus();
}

//agrega los tweet actuales al localstorage
function sincronizarStorage() {
    localStorage.setItem('tweet', JSON.stringify(twet));
}

function borrarTweet(id){
    //el filter me regresa un nuevo arreglo todos menos lo que se parescan al id pasado por parametro
    twet = twet.filter( t=> t.id !== id);
    limpiarHtml();
    crearHtml();
}