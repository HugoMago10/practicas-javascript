//variables
const carrito = document.querySelector("#carrito");//no se reasigna la variable
const contenedorCarrito = document.querySelector("#lista-carrito tbody");
const listaCurso = document.querySelector("#lista-cursos");
const vaciarCarrito = document.querySelector("#vaciar-carrito");
let articulosCarrito = [];

cargarEventListener();
function cargarEventListener(){
    //se agrega un curso al dar click en el boton agregaCarrito
    listaCurso.addEventListener('click', agregarCurso );

    //elimina cursos del carrito
    carrito.addEventListener('click',eliminarCurso);

    //vaciando Carrito
    vaciarCarrito.addEventListener('click', ()=>{
        articulosCarrito = [];
        limpiaHtml();
    });
}

//funciones
function agregarCurso(e){
    e.preventDefault();
    if (e.target.classList.contains('agregar-carrito') ){
        const cursosSeleccionado = e.target.parentElement.parentElement;
        leerDatosCursos( cursosSeleccionado );
    }
}

function eliminarCurso ( e ){
    if (e.target.classList.contains('borrar-curso')){
        const cursoId = e.target.getAttribute('data-id');//cursoId a eliminar

        articulosCarrito.forEach(elemnto=>{
            if ( elemnto.id === cursoId ){
                if ( elemnto.cantidad > 1 )
                    elemnto.cantidad--;
                else{
                    articulosCarrito = articulosCarrito.filter( (cursoCarrito) =>{//regresa un nuevo arreglo sin el cursoId
                        return cursoCarrito.id !== cursoId;
                    });
                }
            }
        });
        carritoHtml();
    }
}

//lee el contenido del html y extrae contenido del curso
function leerDatosCursos( curso ){
    
    //objeto con el contenido del objeto actual
    const infoCurso = {
        imagen: curso.querySelector('img').src,
        titulo: curso.querySelector('h4').textContent,
        precio: curso.querySelector('.precio span').textContent,
        id: curso.querySelector('a').getAttribute('data-id'),
        cantidad: 1
    }

    //revisa si un elemento ya existe en el carrito
    const existe = articulosCarrito.some( curso => curso.id === infoCurso.id );

    if (existe){
        //actualizando la cantidad
        articulosCarrito.map( cur =>{
            cur.id === infoCurso.id ? cur.cantidad++ :'';
        })
    }else{
        //agrega elementos infoCurso al arreglo carrito
        articulosCarrito = [...articulosCarrito, infoCurso];//toma una copia del carrito como esta
    }

    console.log(articulosCarrito);
    carritoHtml();
}

function carritoHtml(){
    limpiaHtml();
    articulosCarrito.forEach ( curso =>{
        //const {imagen, titulo, precio, cantidad, id}=curso //extrae las variables para poderlos usar
        const row = document.createElement('tr');
        row.innerHTML =`
            <td>
                <img src='${curso.imagen}' width='100px'>
            </td>
            <td>${curso.titulo}</td>
            <td>${curso.precio}</td>
            <td>${curso.cantidad}</td>
            <td>
                <a href="#" class='borrar-curso' data-id='${curso.id}'>X</a>
            </td>
        `;
        //agrega html a carrito en tbody
        contenedorCarrito.appendChild(row);
    })
}
//elimina los cursos de tbody, para que no se encimen
function limpiaHtml(){
    //forma lenta
    //contenedorCarrito.innerHTML="";

    /*mientras haya un hijo */
    while(contenedorCarrito.firstChild){
        //el padre elimina su primer hijo
        contenedorCarrito.removeChild( contenedorCarrito.firstChild );
    }
}

