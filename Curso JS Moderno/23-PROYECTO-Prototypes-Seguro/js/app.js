//constructores
function Seguro (marca, year, tipo){
    this.marca = marca;
    this.year = year;
    this.tipo = tipo;
}

//realiza la cotizacion con los datos
Seguro.prototype.cotiza = function(){
    let cantidad;
    let base = 2000;//base de precio 
    switch(this.marca){
        case '1':
            cantidad = base * 1.15;
            break;
        case '2':
            cantidad = base * 1.05;
             break;
        case '3':
            cantidad = base * 1.135;
            break;
        default :
            cantidad = base*1;
            break;
    }
    //leyendo el año, cada año que es mayor se reduce el valor del seguro
    const diferencia = new Date().getFullYear() - this.year;

    cantidad -= ((diferencia*3) * cantidad )/100;

    /**
     * Si el seguro es basico se multiplica por un 30% mas
     * Si el seguro es completo se multiplica por un 50% mas
     * 
     */

    if (this.tipo === 'basico'){
        cantidad = cantidad * 1.30
    }else{
        cantidad *= 1.50
    }

    return cantidad;
}

function UserInterfaces(){
}

UserInterfaces.prototype.llenarOption =()=>{
    const max = new Date().getFullYear(),
    min = max-10;

    const select = document.querySelector('#year');

    for (let i = max;  i>min; i--){
        let opt = document.createElement('option')
        opt.value=i;
        opt.textContent = i;
        select.appendChild(opt);
    }
}

//muestra alertas en pantalla
UserInterfaces.prototype.alertas = (mensaje, tipo)=>{
    const div = document.createElement("div");
    if (tipo==='error'){
        div.classList.add('error')
    }else
        div.classList.add('correcto')
    div.classList.add('mensaje','mt-10');
    div.textContent = mensaje;

    //insertando en html
    const formulario = document.querySelector("#cotizar-seguro");
    formulario.insertBefore(div, document.querySelector('#resultado'));

    setTimeout(()=>{
        div.remove()
    },2000);
}

UserInterfaces.prototype.mostrarResultado = (seguro, total)=>{
    const {marca, year, tipo}=seguro;
    
    //creando resultado
    const div = document.createElement('div');
    div.classList.add('mt-10');

    div.innerHTML = `
        <p class="header">Tu Resumen</p>
        <p class="font-bold">Marca: <span class='font-normal'>${marca==='1'?'Americano':marca==='2'?'Asiatico':'Europeo'}</span></p>
        <p class="font-bold">Año: <span class='font-normal'>${year}</span></p>
        <p class="font-bold">Tipo: <span class='font-normal'>${tipo}</span></p>
        <p class="font-bold">Total: <span class='font-normal'>$${total}</span></p>

    `;
    const resultadoDiv = document.querySelector('#resultado');

    //mostrando el spinner
    const spinner = document.querySelector('#cargando');
    spinner.style.display = 'block';

    setTimeout(()=>{//se borra el spinner despues de 3 seg
        spinner.style.display = 'none';
        resultadoDiv.appendChild(div);//despues aparece el div
    },2000)
}

const ui = new UserInterfaces();
console.log(ui)

document.addEventListener('DOMContentLoaded', ()=>{
    ui.llenarOption();
})

eventListener();
function eventListener(){
    const formulario = document.querySelector("#cotizar-seguro");
    formulario.addEventListener("submit", cotizarSeguro);
}

function cotizarSeguro(e){
    e.preventDefault();

    //leyendo la marca seleccionada
    const marca = document.querySelector("#marca").value;

    //leyendo el año seleccionado
    const year = document.querySelector("#year").value;

    //leyendo el tipo de cobertura
    const tipo = document.querySelector('input[name="tipo"]:checked').value

    if (marca === "" || year==="" || tipo===""){
        ui.alertas("no paso la validacion",'error');
        return;//para que no pase a la cotizacion
    }
    ui.alertas("cotizando seguro",'correcto')

    //ocultando las cotizacion previas
    const res = document.querySelector('#resultado div');
    //verifica si hay un div previo dentro del resultado
    if (res != null ){
        res.remove();
    }

    const seguro = new Seguro(marca, year, tipo);
    const total = seguro.cotiza();

    ui.mostrarResultado(seguro, total);
}