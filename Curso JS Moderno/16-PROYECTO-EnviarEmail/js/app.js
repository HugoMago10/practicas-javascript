//variables
const btnEnviar = document.querySelector('#enviar');
const btnReset = document.querySelector('#resetBtn');
const email = document.querySelector('#email');
const asunto = document.querySelector('#asunto');
const mensaje = document.querySelector('#mensaje');
const form = document.querySelector('#enviar-mail');

eventos();

function eventos(){

    document.addEventListener('DOMContentLoaded', iniciarApp );//inicia hasta que la pagina html se carga

    //campos del formulario
    eventosCampos(email,asunto,mensaje);//evento cuando pierde el focus

    //reinicia formulario
    btnReset.addEventListener('click', reseteaFormulario);

    //enviando email
    form.addEventListener('submit', enviarEmail)
}

//**************************** funciones*******************************************
function iniciarApp(){
    btnEnviar.disabled=true;
}

//valida el formulario
function validarFormulario(e){

    const expRegEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ( e.target.value.length > 0 ){//si los campos tiene texto
        const error = document.querySelector('p.error');
        if (error){
            error.remove();
        }
        if ( e.target.type !=='email'){//si el evento no es un iput email
            //si hay texto pero de otros campos
            e.target.classList.remove('border', 'border-red-500');
            e.target.classList.add('border', 'border-green-500');
        }else{//si el evento es un iput email
            //se valida la expresion regular con el metodo test 
            if ( expRegEmail.test(e.target.value) ) {
                e.target.classList.remove('border', 'border-red-500');
                e.target.classList.add('border', 'border-green-500');
            }else{//si el formato del email no es valido
                e.target.classList.remove('border', 'border-green-500');
                e.target.classList.add('border', 'border-red-500');
                mostrarError("email no valido");
            }
        }
        
    }else{//si el campo no tiene texto
        //e.target.classList.add('border', 'border-red-500');
        mostrarError ('Todos los campos son obligatorios');
        e.target.classList.remove('border', 'border-green-500');
        e.target.classList.add('border', 'border-red-500');
    }

    //verificando de nuevo todos los campos que no tengan border rojo
    const errores = document.querySelectorAll('.border-red-500').length;
    if ( errores != 0 ){
        const camposConErrores = document.querySelectorAll('.border-red-500')[0].type;
        mostrarError("El campo "+camposConErrores+" debe ser valido");
    }else{
        btnEnviar.disabled = false;
        btnEnviar.classList.remove('cursor-not-allowed','opacity-50');
    }

}

function mostrarError(msn){//crea una etiqueta p le agrega estilos y lo agrega al formulario
    const msnError = document.createElement('p');
    msnError.textContent = msn;
    msnError.classList.add('border', 'border-500', 'background-red-100', 'text-red-500','p-3','mt-5','text-center','error');
    const errores = document.querySelectorAll('.error');

    if (errores.length === 0 )
        form.appendChild(msnError);
    
}

function eventosCampos(...input) {
    input.forEach( entrada =>{
        entrada.addEventListener('blur', validarFormulario);
    })
}

function enviarEmail(e){
    e.preventDefault();
    const spinner = document.querySelector('#spinner');
    spinner.style.display = 'flex';

    //despues de 3seg ocultar el spinner y mostrar mensaje
    setTimeout(()=>{
        spinner.style.display='none';

        //mensaje que dice que se envio correctamente
        const parrafo = document.createElement('p');
        parrafo.textContent = 'El mensaje ha sido enviado correctamente';
        parrafo.classList.add('text-center','my-10', 'p-2','bg-green-500','text-white', 'font-bold','uppercase');
        form.insertBefore(parrafo, spinner);//inserta parrafo antes del spinner

        setTimeout(()=>{
            parrafo.remove();
            reseteaFormulario();
        }, 5000 );

    }, 3000);//ejecutar despues de 3 segundos
}

function reseteaFormulario(){
    form.reset();//resetea el formulario
}