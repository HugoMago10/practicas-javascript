//se obtiene el elemento con id=resultado del html
const marca = document.querySelector("#marca");
const year = document.querySelector("#year");
const minimo = document.querySelector("#minimo");
const maximo = document.querySelector("#maximo");
const puertas = document.querySelector("#puertas");
const transmision = document.querySelector("#transmision");
const color = document.querySelector("#color");

//contenedor que contendra los reglones de autos
const resultado = document.querySelector("#resultado")
const yearNow = new Date().getFullYear();//obtiene año actual
const yearMin = yearNow - 10;//año actual menos diez años

//objeto a buscar, con la informacion que seleccione el usuario
const carroBuscar = {
    marca: '',
    modelo: '',
    year: '',
    minimo: '',
    maximo: '',
    puertas: '',
    color: '',
    transmision: ''
}


/**
 * Una vez que cargue el documento html se manda a llamar la funcion mostrarAutos
 */
document.addEventListener('DOMContentLoaded', () => {
    //muestra los automoviles al cargar. estos autos estan en el primer script
    mostrarAutos(autos);

    //llena las opciones del año
    llenaSelect();
});

function mostrarAutos(autos) {

    limpiaAutos();

    //recorre el arreglo autos
    autos.forEach(auto => {
        //creando un parrafo
        const autoHtml = document.createElement('p');

        //se extraen los atributos del objeto auto
        const { marca, modelo, year, puertas, transmision, precio, color } = auto;

        autoHtml.textContent = `
            ${marca} - ${modelo} - ${year} - ${puertas} - ${transmision} - ${precio} - ${color}
        `

        //se inserta el parrafo al elemento html
        resultado.appendChild(autoHtml);
    })
}

//limpiaHtml el html anterior
function limpiaAutos(){
    while( resultado.firstChild ){
        resultado.removeChild( resultado.firstChild );
    }
}

//genera los años del select
function llenaSelect() {
    for (let i = yearNow; i >= yearMin; i--) {
        const opcion = document.createElement('option');
        opcion.value = i;
        opcion.textContent = i;
        year.appendChild(opcion);//agrega las opciones del año al select
    }
}


marca.addEventListener('change', e => {
    carroBuscar.marca = e.target.value;
    filtrarAuto();
});
year.addEventListener('change', e => {
    carroBuscar.year = parseInt(e.target.value);
    filtrarAuto();
});
minimo.addEventListener('change', e => {
    carroBuscar.minimo = e.target.value;
    filtrarAuto();
});
maximo.addEventListener('change', e => {
    carroBuscar.maximo = e.target.value;
    filtrarAuto();
});
puertas.addEventListener('change', e => {
    carroBuscar.puertas = parseInt( e.target.value );
    filtrarAuto();
});
transmision.addEventListener('change', e => {
    carroBuscar.transmision = e.target.value;
    filtrarAuto();
});
color.addEventListener('change', e => {
    carroBuscar.color = e.target.value;
    filtrarAuto();
});

function filtrarAuto(){
    console.log ( carroBuscar );
    //los filtros regresan un auto
    const res = autos
    .filter( filtraMarca )//filtra primero por la marca
    .filter( filtarYear )//despues por el año
    .filter( filtraMinimo )
    .filter( filtraMaximo )
    .filter( filtrarPuerta )
    .filter( filtrarTransmision )
    .filter( filtrarColor );

    if ( res.length )
        mostrarAutos(res);//pasa el auto regresado
    else{
        noEncontroResultado()
    }
}

function noEncontroResultado(){
    limpiaAutos();
    const noResultado = document.createElement('div');
    noResultado.classList.add('alerta', 'error');
    noResultado.textContent = "No se encontraron autos con esas caracterisitcas";
    resultado.appendChild( noResultado );
}

function filtraMarca( auto ){
    const {marca} = carroBuscar;
    if (marca){
        //regreso el auto que sea igual
        return auto.marca === marca;
    }//regreso todos los autos
    return auto;
}

function filtarYear (auto){
    const {year} = carroBuscar;
    if (year){
        return auto.year === year;
    }

    return auto;
}

//obtiene el elemento actual del filter
function filtraMinimo( auto ){
    const {minimo} = carroBuscar;
    if (minimo){
        return auto.precio >= minimo;
    }
    return auto;
}

function filtraMaximo (auto){
    const {maximo} = carroBuscar;
    if (maximo){
        return auto.precio <= maximo;
    }
    return auto;
}

function filtrarPuerta(auto){
    const {puertas} = carroBuscar;
    if (puertas){
        return auto.puertas === puertas;
    }
    return auto;
}

function filtrarTransmision(auto){
    const {transmision} = carroBuscar;
    if (transmision){
        return auto.transmision === transmision;
    }
    return auto;
}

function filtrarColor(auto){
    const {color} = carroBuscar;
    if (color){
        return auto.color === color;
    }
    return auto;
}