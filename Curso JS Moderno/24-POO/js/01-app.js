class Cliente{
    constructor (nombre='', saldo=0.0){
        this._nombre = nombre;
        this._saldo = saldo;
    }

    mostrarInformacion(){
        return `Cliente: ${this._nombre} tu saldo es de: ${this._saldo}`
    }
}

const juan = new Cliente('Juan', 2500);
console.log ( juan.mostrarInformacion() );

const juan1 = new Cliente();
console.log ( juan1.mostrarInformacion() );

const Cliente2 = class{
    constructor(nombre, saldo){
        this.nombre = nombre;
        this.saldo = saldo;
    }
}

const maria = new Cliente2("Maria", 520);
console.log (maria);

const maria1 = new Cliente2("Maria Juana", 620);
console.log (maria1);