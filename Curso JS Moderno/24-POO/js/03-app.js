class Cliente{
    static tipo= 'persona';
    constructor (nombre='', saldo=0.0){
        this._nombre = nombre;
        this._saldo = saldo;
    }

    mostrarInformacion(){
        return `Cliente: ${this._nombre} tu saldo es de: ${this._saldo}`
    }

    static getTipo(){
        return this.tipo;
    }
}

//herencia
class ClienteFrecuente extends Cliente{
    static tipo = 'Frecuente'

    constructor(nombre, saldo, telefono){
        super(nombre, saldo);
        this._telefono = telefono;
    }
}

const juan = new Cliente ('juan',500);//cliente
console.log ( Cliente.getTipo() );
console.log (juan);

const pedro = new ClienteFrecuente ('pedro',750,'412-545-785');//cliente frecuente
console.log ( ClienteFrecuente.getTipo() );
console.log (pedro);

