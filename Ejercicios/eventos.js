function toMayuscucla(){
    var texto = document.querySelector('.entrada').value;
    document.querySelector('textarea').textContent = texto.toUpperCase();
}

function iniciar (){

    //*************** forma1 de dar eventos ****************************
    var input = document.querySelector('.entrada');
    input.addEventListener('keyup', toMayuscucla, false);

    //***************** forma2 de dar eventos ****************************
    var textArea = document.querySelector('textarea');
    textArea.addEventListener('click', ()=>{
        document.querySelector('span').textContent = textArea.value.length;
    });

    //****** forma3 de dar eventos */
    document.querySelector('p').onclick = ()=>{
        document.querySelector('p').style.color = 'blue';
    }
}


window.addEventListener("load", iniciar, false);