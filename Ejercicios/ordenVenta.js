class Producto {
    
    static contador = 0;

    constructor (nombre, precio){
        this._id = ++Producto.contador;
        this._nombre = nombre;
        this._precio = precio;
    }

    get id(){ return this._id; }
    get nombre (){ return this._nombre; }
    get precio (){ return this._precio; }

    set nombre (nombre){ this._nombre = nombre; }
    set precio (precio){ this._precio = precio; }

    toString (){
        return `\nid: ${this.id} \nnombre: ${this.nombre} \nprecio: ${this.precio}`;
    }
}

//creando la relacion un producto se agrega a una orden
class Orden{
    static contadorOrden = 0;//se declara una variable statica

    static get MAX_PRODUCTO(){ return 5; }//declarando una constante

    constructor (){
        this._noOrden = ++Orden.contadorOrden;//se accede de la Clase
        this._productos = [];
    }

    get noOrden (){//para poder usar el get, el nombre debe ser igual al del atributo
        return this._noOrden;
    }

    addProducto ( producto ){
        if ( this._productos.length <= Orden.MAX_PRODUCTO ){
            this._productos.push ( producto );
        }else
            console.log ("Productos superados, no se pueden agregar mas");
    }

    calcularTotal (){
        let totalVenta = 0;
        for ( let producto of this._productos ){
            totalVenta += producto.precio;
        }
        return totalVenta;
    }

    mostrarOrden (){
        let productoOrden = '';

        for (let prod of this._productos ){
            productoOrden += prod.toString()+ ' ';
        }
        console.log(` n° de Orden: ${this.noOrden}  \nProductos: ${productoOrden}\nTotal: ${this.calcularTotal()}` );
    }
}

function start (){
    let producto1 = new Producto ("Camisa", 455);
    let producto2 = new Producto ("Pantalon", 635);
    let producto3 = new Producto ("Sueter", 685);
    let producto4 = new Producto ("Short", 333);
    let producto5 = new Producto ("Tennis", 982);

    let orden1 = new Orden ();
    orden1.addProducto( producto1 );
    orden1.addProducto( producto2 );
    orden1.addProducto( producto3 );
    orden1.mostrarOrden ();

    let orden2 = new Orden ();
    orden2.addProducto( producto4 );
    orden2.addProducto( producto5 );
    orden2.mostrarOrden ();

    let orden3 = new Orden ();
    orden3.addProducto( producto1 );
    orden3.addProducto( producto3 );
    orden3.addProducto( producto5 );
    orden3.mostrarOrden ();
}

window.addEventListener( "load", start, false );