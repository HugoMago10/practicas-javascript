function iniciar (){

    /************************************** map ************************/
    /*El metodo map crea un nuevo arreglo */
    var numeros = [1,3,5,7,9];

    var double = numeros.map ( (x)=>{//regresa un nuevo arreglo con los nuevos nuevos
        return x*2;
    })

    console.log ( double );
    double.forEach ( elem => console.log ( elem ) );//imprime cada elemento

    /******************** reduce ***********************************/
    const resultado = double.reduce ( ( primerElem, segundoElem ) =>{
        return primerElem += segundoElem;
    });
    console.log("res: "+resultado );

    const nombres=[
            'Miguel','Andrea','Wendy','Diana','Jose',
            'Diana','Jose','Diana'
        ];
    const cantNombres = nombres.reduce( function (x,y) {
        x = x.toUpperCase()+'*'+y;
        return x;
    });

    console.log (cantNombres);

    const num = [1, 3, 5, 7, 9, 11];
    const suma = num.reduce ( (a,b) => { return a+b})
    console.log ("suma reduce: "+suma);

    /***************************** some() *******************/
    /**comprueba si al menos un elemento del array cumple con la condición implementada por la función proporcionada.*/
    var array = [1,2,3,4,5];//contiene al menos un elemento par
    const even = (element)=> element%2===0;
    console.log ( array.some ( even ) );
    array = [1,3,5,7,9];//no contiene ningun elemento par
    console.log ( array.some ( even ) );
    // otra forma de usar some 
    console.log ( array.some ( (x)=>{
        return x % 2 !== 0;
    }))


    /******* ***/
    console.log ("******************** sintaxis spread****************");

    let persona = {}//declarando un objeto
    var arreglo1 = [1,2,3,4];
    var copiaArreglo1 = [...arreglo1, persona]//como arreglo1.slice() -> copia arreglo1 y objeto persona

    persona.nombre = 'juan';
    persona.apellido = 'lopez';
    console.log ( copiaArreglo1);

    console.log ( [1,3,6,5,10].some (x=> x > 9));//devuelve true si existe al menos un elemento mayor a 9

    /************************** objetos y arreglos ************/
    var a = [[1],[2],[3]];
    var b = [...a];
    console.log ( b.shift().shift() );
    console.log ( b );
}

window.addEventListener("load", iniciar, false );