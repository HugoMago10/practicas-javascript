class Persona{
    
    //declaracion de una variable estatica
    static contadorPersona = 0;

    constructor (nombre, apellido, edad){
        this._id = ++Persona.contadorPersona;
        this._nombre = nombre;
        this._apellido = apellido;
        this._edad = edad;
    }

    set nombre (nombre){ this._nombre = nombre; }
    set apellido (apellido){ this._apellido = apellido; }
    set edad (edad){ this._edad = edad; }

    get id(){ return this._id; }
    get nombre (){ return this._nombre; }
    get apellido(){ return this._apellido; }
    get edad (){ return this._edad; }

    toString (){
        //this.nombre, this.apellido y this.edad, acceden a los metodos de la clase
        return `id: ${this._id} nombre: ${this.nombre} apellido: ${this.apellido} edad: ${this.edad}`;
    }
}

class Empleado extends Persona{
    
    constructor (nombre, apellido, edad, sueldo){
        super (nombre, apellido, edad);
        this._sueldo = sueldo;
    }

    set sueldo (sueldo){ this._sueldo = sueldo; }

    get sueldo (){
        return this._sueldo;
    }

    toString(){
        return `${super.toString()} sueldo: ${this.sueldo}`;
    }
}

class Cliente extends Persona{
    constructor (nombre, apellido, edad, fechaRegistro){
        super(nombre, apellido, edad);
        this._fechaRegistro = fechaRegistro;
    }

    set fechaRegistro (fechaRegistro){
        this._fechaRegistro=fechaRegistro;
    }
    get fechaRegistro (){
        return this._fechaRegistro;
    }

    toString(){
        return `${super.toString()} fecha Registro: ${this.fechaRegistro}`;
    }

}

//inicia con la prueba de las clases
function start()
{
    //prueba clase persona
    let persona = new Persona ("Juan","Miguel Sanchez", 32);
    console.log ( persona.toString () );

    let empleado1 = new Empleado ("Juan", "Perez Lopez", 23, 3000);
    console.log ( empleado1.toString () );

    let cliente1 = new Cliente("Miguel","Zavaleta Mendez", 27, new Date () );
    console.log ( cliente1.toString () );
}

//espera a que se termine de ejecutar el archivo html y llama al metodo start
window.addEventListener( "load", start, false );