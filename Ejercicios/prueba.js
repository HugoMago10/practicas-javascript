class Usuario {
    constructor(username, password, email) {
        this._username = username;
        this._password = password;
        this._email = password;
    }

    set username(username) { this._username = username; }
    set password(pass) { this._pass = pass; }
    set email(email) { this._email = email; }
    get username() { return this._username; }
    get password() { return this._password; }
    get email() { return this._email; }

    toString(row) {
        return `${this.username} ${this.password} ${this.email}`;
    }

    getRowHtml() {
        var tr = document.createElement("tr");
        var tdUsername = document.createElement("td");
        var tdPassword = document.createElement("td");
        var tdEmail = document.createElement("td");
        tdUsername.textContent = this.username;
        tdPassword.textContent = this.password;
        tdEmail.textContent = this.email;
        tr.appendChild(tdUsername);
        tr.appendChild(tdPassword);
        tr.appendChild(tdEmail);
        return tr;
    }
}

function start() {
    tabla = document.getElementById("miTabla");

    let u1 = new Usuario("Juan", "1234", "Mendez");
    let u2 = new Usuario("Maria", "1234", "Lopez");
    let u3 = new Usuario("Jose", "1234", "Sanchez");

    tabla.appendChild( u1.getRowHtml() )
    tabla.appendChild( u2.getRowHtml() );
    tabla.appendChild( u3.getRowHtml() );

    /*tabla.appendChild( imprimeFilas(u1) )
    tabla.appendChild( imprimeFilas(u2 ));
    tabla.appendChild( imprimeFilas(u3 ));*/
}

function imprimeFilas(usuario) {
    var tr = document.createElement("tr");
    var tdUsername = document.createElement("td");
    var tdPassword = document.createElement("td");
    var tdEmail = document.createElement("td");
    tdUsername.textContent = usuario.username;
    tdPassword.textContent = usuario.password;
    tdEmail.textContent = usuario.email;
    tr.appendChild(tdUsername);
    tr.appendChild(tdPassword);
    tr.appendChild(tdEmail);
    return tr;
}


window.addEventListener("load", start, false);